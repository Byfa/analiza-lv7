using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace xoxo
{
    public partial class Form1 : Form
    {
        //varijable za rezultat unutar pictureboxa, 0-nista, 1-kruzic, 2-x
        int r00 = 0, r01 = 0, r02 = 0, r10 = 0, r11 = 0, r12 = 0, r20 = 0, r21 = 0, r22 = 0;
        //varijable za provjeru prvog klika na picture box, zabrana ponovnog crtanja
        bool m00 = false, m01 = false, m02 = false, m10 = false, m11 = false, m12 = false, m20 = false, m21 = false, m22 = false;

        Graphics g00, g01, g02, g10, g11, g12, g20, g21, g22;

        //provjera je li ime uneseno
        bool nchg1 =false, nchg2 = false;

        //provjera je li program gotov, tj pobjednik odabran
        bool finished = false;

        //brojaci pobjeda
        int O_cnt = 0, X_cnt = 0, D_cnt = 0;

        //bool kako bi prvi igrac uvijek bio p1, X
        bool player = true;

        Pen p = new Pen(Color.Red, 1.0f);

        IShape s; 

        public Form1()
        {
            InitializeComponent();
            g00 = pB00.CreateGraphics();
            g01 = pB01.CreateGraphics();
            g02 = pB02.CreateGraphics();
            g10 = pB10.CreateGraphics();
            g11 = pB11.CreateGraphics();
            g12 = pB12.CreateGraphics();
            g20 = pB20.CreateGraphics();
            g21 = pB21.CreateGraphics();
            g22 = pB22.CreateGraphics();
        }
        private void pB_Click(object sender, EventArgs e)
        {
            //ako je p1 onda pravi O ako ne onda X
            if (player)
            {
                turn_text.Text = p2_rez_name.Text;
                s = new X();
            }
            else
            {
                turn_text.Text = p1__rez_name.Text;
                s = new Circle();
            }

            //promjeni igraca
            player = !player;

            //ako su imena unesena, promjeni ime
            if(nchg1)
                p1__rez_name.Text = player1_name.Text;
            if (nchg2)
                p2_rez_name.Text = player2_name.Text;
        }

        //provjera promjena imena
        private void player1_name_TextChanged(object sender, EventArgs e)
        {
            nchg1 = true;
        }
        private void player2_name_TextChanged(object sender, EventArgs e)
        {
            nchg2 = true;
        }

        //unos X ili O
        private void pB00_MouseUp(object sender, MouseEventArgs e)
        {
            //ako do sada nije vec iskoristena kucica, upisi
            if (!m00)
            {
                s.Draw(g00, p, e.X, e.Y);
                m00 = !m00;
                //ovisno o igracu, stavljamo koji je crtez unutra kucice
                if (player) r00 = 1;
                else r00 = 2;
            }
            //provjera pobjede
            win_check();
        }
        private void pB01_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m01)
            {
                s.Draw(g01, p, e.X, e.Y);
                m01 = !m01;
                if (player) r01 = 1;
                else r01 = 2;
            }
            win_check();
        }
        private void pB02_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m02)
            {
                s.Draw(g02, p, e.X, e.Y);
                m02 = !m02;
                if (player) r02 = 1;
                else r02 = 2;
            }
            win_check();
        }
        private void pB10_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m10)
            {
                s.Draw(g10, p, e.X, e.Y);
                m10 = !m10;
                if (player) r10 = 1;
                else r10 = 2;
            }
            win_check();
        }
        private void pB11_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m11)
            {
                s.Draw(g11, p, e.X, e.Y);
                m11 = !m11;
                if (player) r11 = 1;
                else r11 = 2;
            }
            win_check();
        }
        private void pB12_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m12)
            {
                s.Draw(g12, p, e.X, e.Y);
                m12 = !m12;
                if (player) r12 = 1;
                else r12 = 2;
            }
            win_check();
        }
        private void pB20_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m20)
            {
                s.Draw(g20, p, e.X, e.Y);
                m20 = !m20;
                if (player) r20 = 1;
                else r20 = 2;
            }
            win_check();
        }
        private void pB21_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m21)
            {
                s.Draw(g21, p, e.X, e.Y);
                m21 = !m21;
                if (player) r21 = 1;
                else r21 = 2;
            }
            win_check();
        }
        private void pB22_MouseUp(object sender, MouseEventArgs e)
        {
            if (!m22)
            {
                s.Draw(g22, p, e.X, e.Y);
                m22 = !m22;
                if (player) r22 = 1;
                else r22 = 2;
            }
            win_check();
        }

        private void start_Click(object sender, EventArgs e)
        {
            //resetiranje pozadine svakog picture boxa, tj prebojamo
            g00.Clear(Color.White);
            g01.Clear(Color.White);
            g02.Clear(Color.White);
            g10.Clear(Color.White);
            g11.Clear(Color.White);
            g12.Clear(Color.White);
            g20.Clear(Color.White);
            g21.Clear(Color.White);
            g22.Clear(Color.White);

            //resetiranje klikova na svakj kucici
            m00 = false;
            m01 = false;
            m02 = false; 
            m10 = false; 
            m11 = false; 
            m12 = false; 
            m20 = false;
            m21 = false;
            m22 = false;

            //resetiranje crteza u kucicama
            r00 = 0;
            r01 = 0;
            r02 = 0;
            r10 = 0;
            r11 = 0;
            r12 = 0;
            r20 = 0;
            r21 = 0;
            r22 = 0;

            //igra je resetirana

            finished = false;
            //ponovno stavljano prvi igrac da je player1
            player = true;
            turn_text.Text = p1__rez_name.Text;
        }

        private void finish()
        {
            //zabrana klikova
            m00 = true;
            m01 = true;
            m02 = true;
            m10 = true;
            m11 = true;
            m12 = true;
            m20 = true;
            m21 = true;
            m22 = true;

            //igra je gotova, ne moze se vise nista raditi
            finished = true;
        }

        private void win_check()
        {
            //uvjeti pobjeda
            if ((r00 == 1 && r01 == 1 && r02 == 1) || (r10 == 1 && r11 == 1 && r12 == 1) || (r20 == 1 && r21 == 1 && r22 == 1) || (r00 == 1 && r10 == 1 && r20 == 1) || (r01 == 1 && r11 == 1 && r21 == 1) || (r02 == 1 && r12 == 1 && r22 == 1) || (r00 == 1 && r11 == 1 && r22 == 1) || (r02 == 1 && r11 == 1 && r20 == 1))
            {
                if (finished == false)
                    O_cnt++;
                //zavrsavanje igre
                finish();
                //prompt pobjednika
                MessageBox.Show(p1__rez_name.Text + " wins!");
            }
            else if ((r00 == 2 && r01 == 2 && r02 == 2) || (r10 == 2 && r11 == 2 && r12 == 2) || (r20 == 2 && r21 == 2 && r22 == 2) || (r00 == 2 && r10 == 2 && r20 == 2) || (r01 == 2 && r11 == 2 && r21 == 2) || (r02 == 2 && r12 == 2 && r22 == 2) || (r00 == 2 && r11 == 2 && r22 == 2) || (r02 == 2 && r11 == 2 && r20 == 2))
            {
                if (finished == false)
                    X_cnt++;
                finish();
                MessageBox.Show(p2_rez_name.Text + " wins!");
            }
            else if (r00 != 0 && r01 != 0 && r02 != 0 && r10 != 0 && r20 != 0 && r11 != 0 && r21 != 0 && r12 != 0 && r22 != 0)
            {
                if (finished == false)
                    D_cnt++;
                finish();
                MessageBox.Show("Draw!");
            }
            //promjena rezultat
            p1_rez.Text = O_cnt.ToString();
            p2_rez.Text = X_cnt.ToString();
            draw_rez.Text = D_cnt.ToString();
        }
    }
}
