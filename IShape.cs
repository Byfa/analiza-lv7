using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xoxo
{
    interface IShape
    {
        void Draw(Graphics g, Pen p, int x, int y);
    }

    //potrebni su nam samo krug i X
    class Circle : IShape
    {
        int r = 25;

        public void Draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x-10, y-10, r, r);
        }
    }

    //X kreiramo tako da napravimo dvije linije koje se sjeku u sredistu danih koordinata
    class X : IShape
    {
        public void Draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawLine(p, x - 10, y + 10, x + 10, y - 10);
            g.DrawLine(p, x - 10, y - 10, x + 10, y + 10);
        }
    }
}
