namespace xoxo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.start = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.player1_name = new System.Windows.Forms.TextBox();
            this.player2_name = new System.Windows.Forms.TextBox();
            this.pB00 = new System.Windows.Forms.PictureBox();
            this.pB10 = new System.Windows.Forms.PictureBox();
            this.pB20 = new System.Windows.Forms.PictureBox();
            this.pB11 = new System.Windows.Forms.PictureBox();
            this.pB12 = new System.Windows.Forms.PictureBox();
            this.pB01 = new System.Windows.Forms.PictureBox();
            this.pB21 = new System.Windows.Forms.PictureBox();
            this.pB02 = new System.Windows.Forms.PictureBox();
            this.pB22 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.p1__rez_name = new System.Windows.Forms.Label();
            this.p2_rez_name = new System.Windows.Forms.Label();
            this.draw = new System.Windows.Forms.Label();
            this.p1_rez = new System.Windows.Forms.Label();
            this.p2_rez = new System.Windows.Forms.Label();
            this.draw_rez = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.turn_text = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pB00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB22)).BeginInit();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(225, 324);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 0;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Player1: X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Player2: O";
            // 
            // player1_name
            // 
            this.player1_name.Location = new System.Drawing.Point(12, 35);
            this.player1_name.Name = "player1_name";
            this.player1_name.Size = new System.Drawing.Size(100, 20);
            this.player1_name.TabIndex = 3;
            this.player1_name.TextChanged += new System.EventHandler(this.player1_name_TextChanged);
            // 
            // player2_name
            // 
            this.player2_name.Location = new System.Drawing.Point(200, 35);
            this.player2_name.Name = "player2_name";
            this.player2_name.Size = new System.Drawing.Size(100, 20);
            this.player2_name.TabIndex = 4;
            this.player2_name.TextChanged += new System.EventHandler(this.player2_name_TextChanged);
            // 
            // pB00
            // 
            this.pB00.BackColor = System.Drawing.SystemColors.Window;
            this.pB00.Location = new System.Drawing.Point(67, 86);
            this.pB00.Name = "pB00";
            this.pB00.Size = new System.Drawing.Size(45, 45);
            this.pB00.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pB00.TabIndex = 5;
            this.pB00.TabStop = false;
            this.pB00.Click += new System.EventHandler(this.pB_Click);
            this.pB00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB00_MouseUp);
            // 
            // pB10
            // 
            this.pB10.BackColor = System.Drawing.SystemColors.Window;
            this.pB10.Location = new System.Drawing.Point(67, 142);
            this.pB10.Name = "pB10";
            this.pB10.Size = new System.Drawing.Size(45, 45);
            this.pB10.TabIndex = 6;
            this.pB10.TabStop = false;
            this.pB10.Click += new System.EventHandler(this.pB_Click);
            this.pB10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB10_MouseUp);
            // 
            // pB20
            // 
            this.pB20.BackColor = System.Drawing.SystemColors.Window;
            this.pB20.Location = new System.Drawing.Point(67, 196);
            this.pB20.Name = "pB20";
            this.pB20.Size = new System.Drawing.Size(45, 45);
            this.pB20.TabIndex = 7;
            this.pB20.TabStop = false;
            this.pB20.Click += new System.EventHandler(this.pB_Click);
            this.pB20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB20_MouseUp);
            // 
            // pB11
            // 
            this.pB11.BackColor = System.Drawing.SystemColors.Window;
            this.pB11.Location = new System.Drawing.Point(134, 142);
            this.pB11.Name = "pB11";
            this.pB11.Size = new System.Drawing.Size(45, 45);
            this.pB11.TabIndex = 8;
            this.pB11.TabStop = false;
            this.pB11.Click += new System.EventHandler(this.pB_Click);
            this.pB11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB11_MouseUp);
            // 
            // pB12
            // 
            this.pB12.BackColor = System.Drawing.SystemColors.Window;
            this.pB12.Location = new System.Drawing.Point(200, 142);
            this.pB12.Name = "pB12";
            this.pB12.Size = new System.Drawing.Size(45, 45);
            this.pB12.TabIndex = 9;
            this.pB12.TabStop = false;
            this.pB12.Click += new System.EventHandler(this.pB_Click);
            this.pB12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB12_MouseUp);
            // 
            // pB01
            // 
            this.pB01.BackColor = System.Drawing.SystemColors.Window;
            this.pB01.Location = new System.Drawing.Point(134, 86);
            this.pB01.Name = "pB01";
            this.pB01.Size = new System.Drawing.Size(45, 45);
            this.pB01.TabIndex = 10;
            this.pB01.TabStop = false;
            this.pB01.Click += new System.EventHandler(this.pB_Click);
            this.pB01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB01_MouseUp);
            // 
            // pB21
            // 
            this.pB21.BackColor = System.Drawing.SystemColors.Window;
            this.pB21.Location = new System.Drawing.Point(134, 196);
            this.pB21.Name = "pB21";
            this.pB21.Size = new System.Drawing.Size(45, 45);
            this.pB21.TabIndex = 11;
            this.pB21.TabStop = false;
            this.pB21.Click += new System.EventHandler(this.pB_Click);
            this.pB21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB21_MouseUp);
            // 
            // pB02
            // 
            this.pB02.BackColor = System.Drawing.SystemColors.Window;
            this.pB02.Location = new System.Drawing.Point(200, 86);
            this.pB02.Name = "pB02";
            this.pB02.Size = new System.Drawing.Size(45, 45);
            this.pB02.TabIndex = 12;
            this.pB02.TabStop = false;
            this.pB02.Click += new System.EventHandler(this.pB_Click);
            this.pB02.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB02_MouseUp);
            // 
            // pB22
            // 
            this.pB22.BackColor = System.Drawing.SystemColors.Window;
            this.pB22.Location = new System.Drawing.Point(200, 196);
            this.pB22.Name = "pB22";
            this.pB22.Size = new System.Drawing.Size(45, 45);
            this.pB22.TabIndex = 13;
            this.pB22.TabStop = false;
            this.pB22.Click += new System.EventHandler(this.pB_Click);
            this.pB22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pB22_MouseUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 246);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Rezultat:";
            // 
            // p1__rez_name
            // 
            this.p1__rez_name.AutoSize = true;
            this.p1__rez_name.Location = new System.Drawing.Point(26, 272);
            this.p1__rez_name.Name = "p1__rez_name";
            this.p1__rez_name.Size = new System.Drawing.Size(45, 13);
            this.p1__rez_name.TabIndex = 15;
            this.p1__rez_name.Text = "Player1:";
            // 
            // p2_rez_name
            // 
            this.p2_rez_name.AutoSize = true;
            this.p2_rez_name.Location = new System.Drawing.Point(26, 305);
            this.p2_rez_name.Name = "p2_rez_name";
            this.p2_rez_name.Size = new System.Drawing.Size(45, 13);
            this.p2_rez_name.TabIndex = 16;
            this.p2_rez_name.Text = "Player2:";
            // 
            // draw
            // 
            this.draw.AutoSize = true;
            this.draw.Location = new System.Drawing.Point(26, 337);
            this.draw.Name = "draw";
            this.draw.Size = new System.Drawing.Size(35, 13);
            this.draw.TabIndex = 17;
            this.draw.Text = "Draw:";
            // 
            // p1_rez
            // 
            this.p1_rez.AutoSize = true;
            this.p1_rez.Location = new System.Drawing.Point(96, 272);
            this.p1_rez.Name = "p1_rez";
            this.p1_rez.Size = new System.Drawing.Size(13, 13);
            this.p1_rez.TabIndex = 18;
            this.p1_rez.Text = "0";
            // 
            // p2_rez
            // 
            this.p2_rez.AutoSize = true;
            this.p2_rez.Location = new System.Drawing.Point(96, 305);
            this.p2_rez.Name = "p2_rez";
            this.p2_rez.Size = new System.Drawing.Size(13, 13);
            this.p2_rez.TabIndex = 19;
            this.p2_rez.Text = "0";
            // 
            // draw_rez
            // 
            this.draw_rez.AutoSize = true;
            this.draw_rez.Location = new System.Drawing.Point(96, 337);
            this.draw_rez.Name = "draw_rez";
            this.draw_rez.Size = new System.Drawing.Size(13, 13);
            this.draw_rez.TabIndex = 20;
            this.draw_rez.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Playing:";
            // 
            // turn_text
            // 
            this.turn_text.AutoSize = true;
            this.turn_text.Location = new System.Drawing.Point(135, 58);
            this.turn_text.Name = "turn_text";
            this.turn_text.Size = new System.Drawing.Size(42, 13);
            this.turn_text.TabIndex = 22;
            this.turn_text.Text = "Player1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 375);
            this.Controls.Add(this.turn_text);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.draw_rez);
            this.Controls.Add(this.p2_rez);
            this.Controls.Add(this.p1_rez);
            this.Controls.Add(this.draw);
            this.Controls.Add(this.p2_rez_name);
            this.Controls.Add(this.p1__rez_name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pB22);
            this.Controls.Add(this.pB02);
            this.Controls.Add(this.pB21);
            this.Controls.Add(this.pB01);
            this.Controls.Add(this.pB12);
            this.Controls.Add(this.pB11);
            this.Controls.Add(this.pB20);
            this.Controls.Add(this.pB10);
            this.Controls.Add(this.pB00);
            this.Controls.Add(this.player2_name);
            this.Controls.Add(this.player1_name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.start);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pB00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB22)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox player1_name;
        private System.Windows.Forms.TextBox player2_name;
        private System.Windows.Forms.PictureBox pB00;
        private System.Windows.Forms.PictureBox pB10;
        private System.Windows.Forms.PictureBox pB20;
        private System.Windows.Forms.PictureBox pB11;
        private System.Windows.Forms.PictureBox pB12;
        private System.Windows.Forms.PictureBox pB01;
        private System.Windows.Forms.PictureBox pB21;
        private System.Windows.Forms.PictureBox pB02;
        private System.Windows.Forms.PictureBox pB22;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label p1__rez_name;
        private System.Windows.Forms.Label p2_rez_name;
        private System.Windows.Forms.Label draw;
        private System.Windows.Forms.Label p1_rez;
        private System.Windows.Forms.Label p2_rez;
        private System.Windows.Forms.Label draw_rez;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label turn_text;
    }
}

